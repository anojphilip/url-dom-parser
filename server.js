
// Dependencies
var express = require('express');
//var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Express
var app = express();
app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routes
require('./Routes')(app);



app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
