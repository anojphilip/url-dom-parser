exports.findAll = function(req, response){};
exports.findById = function(req, response) {
	var urlString = decodeURIComponent(req.params.id);

	function extractHostname(url) {
	    var hostname;
	    var path
	    var port = 80;
	    var protocol='http';
	    if (url.indexOf("://") > -1) {
	    	protocol = url.split('://')[0];
	        hostname = url.split('/')[2];
	        path = url.split('/')[3] ?  path = url.split('/')[3] :'';
	    }
	    else {
	        hostname = url.split('/')[0];
	        path = url.split('/')[1] ?  path = url.split('/')[1] :'';
	    }

	    if(protocol=='https'){
	    	port = 443;
	    }

	    //find & remove port number
	    hostname = hostname.split(':')[0];

	    //find & remove "?"
	    hostname = hostname.split('?')[0];
	    return {
    			'hostname':hostname,
    			'protocol':protocol,
    			'port':port,
    			'path':'/'+path,
			};

	}

	var hostDetails = extractHostname(urlString);	


	
	var options = {
	  host: hostDetails.hostname,
	  port: hostDetails.port,
	  path: hostDetails.path
	};

	console.log(urlString);
	console.log(options.host+':'+options.port+options.path);
	
	if(hostDetails.protocol=='http'){
		
		var http = require('http');

		http.get(options, function(res) {
			var body = '';
			  res.on('data', function(chunk) {
			    body += chunk;
			  });
			  res.on('end', function() {
			     response.send([{"data": body  }]);
			  });

		}).on('error', function(e) {
		  console.log("Got error: " + e.message);
		  response.send([{"data": 'Could not connect to the requested url'  }]);
		});
	}
	else{
		var https = require('https');

		https.get(urlString,  function(res) {
			var body = '';
			  res.on('data', function(chunk) {
			    body += chunk;
			  });
			  res.on('end', function() {
			     response.send([{"data": body  }]);
			  });

		}).on('error', function(e) {
		  console.log("Got error: " + e.message);
		  response.send([{"data": 'Could not connect to the requested url'  }]);
		});

	}



	

	

};
exports.add = function() {};
exports.update = function() {};
exports.delete = function() {};