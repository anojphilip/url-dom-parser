var app = angular.module('myApp', ['ngResource', 'ngAnimate', 'toaster']);
app.config(function ($httpProvider) {
  $httpProvider.interceptors.push('myHttpInterceptor');
});

// Handle http server errors
app.factory('myHttpInterceptor', function ($q,toaster) {
    return {
        responseError: function (response) {
          console.log(response);
          if(response.data){
            if(response.data.message)
            toaster.error("Error: ", response.data.message);
            else
            toaster.error("Error: ", response.data);
          }
          return $q.reject(response);
        }
    };
});


app.factory('UrlFetcher', function($resource) {
  return $resource('/urldata/:urlId',  {urlId:'@urlId'});
});


app.directive('highlight', function() {
  return {
    restrict: 'EA',
    link: function(scope, element, attr) {
        element.bind('click', function(el){
          console.log(el);
          var className = el.target.className;
          $('.high-light').removeClass("high-light");
          $('.'+className.split(' ')[1]).addClass("high-light");
        });   
    }
  };
});

app.directive('clearData', function() {
  return {
    restrict: 'EA',
    link: function(scope, element, attr) {
        element.bind('click', function(el){
         $( "#contentId" ).empty();
        });   
    }
  };
});

app.directive('sourceTree', function() {
  return {
    restrict: 'EA',
    link: function(scope, element, attr) {
        var $contentId = $( "#contentId" );

        scope.$watch('webContent', function(newVal,oldVal) {
            //var htmlTree = $.parseHTML( newVal,document,true );     // to add script tags as well 
            if(newVal){
                var htmlTree = $.parseHTML( newVal );
                getDomNodes(htmlTree,$contentId); 
            }

        });


        function getDomNodes(html,appendElmnt){
          $.each( html, function( i, el ) {
            let domcCnt,domcCntBefore,domcCntAfter;
            if(el.children && el.children.length!=0){
               domcCntBefore = el.outerHTML.split(el.innerHTML.trim())[0];
               domcCntAfter = el.outerHTML.split(el.innerHTML.trim())[1];
               domcCnt = null;
            }
            else{
              domcCnt  = el.outerHTML?el.outerHTML:el.textContent;
            }
              var $newdivbefore = $('<div class="padding20 '+el.nodeName+'MG"  ></div>');
              var $newdivCont = $('<div class="padding20"  ></div>');
              var $newdivAfter = $('<div class="padding20 '+el.nodeName+'MG"></div>');
              if(domcCnt){
                $newdivbefore.text(domcCnt);
                $newdivCont = null;
                newdivAfter = null;
              }
              else{
                 $newdivbefore.text(domcCntBefore);
               getDomNodes(el.children,$newdivCont);
               $newdivAfter.text(domcCntAfter);
              }
            

            appendElmnt.append($newdivbefore);
            if($newdivAfter){
              appendElmnt.append($newdivCont);
              appendElmnt.append($newdivAfter);
            }

          });

        }

    }
  };
});
