module.exports = function(app){
    var urldata = require('./controllers/urldata');
    app.get('/urldata', urldata.findAll);
    app.get('/urldata/:id', urldata.findById);
    app.post('/urldata', urldata.add);
    app.put('/urldata/:id', urldata.update);
    app.delete('/urldata/:id', urldata.delete);
}